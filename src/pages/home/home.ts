import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {AngularFireDatabase} from '@angular/fire/database';
import { Observable } from 'rxjs-compat';
import {map} from 'rxjs/operators';
import {ActionSheetController} from 'ionic-angular';
import {AngularFireAuth} from 'angularfire2/auth';
import { stringify } from '@angular/compiler/src/util';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  items: Observable<any[]>;

  constructor(public navCtrl: NavController, private db: AngularFireDatabase, private as: ActionSheetController, private af: AngularFireAuth ) {
    
    var result=this.af.auth.signInWithEmailAndPassword(	
      "mp2018f@gmail.com", "mp2018f" ).catch( 
        reason=> alert(reason));

    console.log(result);

    //this.items = this.db.list('shopping-list').valueChanges();

    // this.items = this.db.list('shopping-list').snapshotChanges().pipe(map(changes => 	changes.map(c => 		({ key: c.payload.key, ...c.payload.val() }))));
    
    // this.items.subscribe(x=>console.log(x));
    //this.db.list('shopping-list').push({name: "apple", favorite: "True" });
    //this.db.list('shopping-list').update("1235", {name:"milk", price: 500});
    
    
  }
  login() {

    this.navCtrl.push('TabsPage');
  }

  async register() {	
    await this.af.auth. createUserWithEmailAndPassword(
      "hello@hello.com", "helloworld" )
      .catch( reason=> alert(reason));
  }

  onDelete(item) {
    this.db.list('shopping-list').remove(item.key);
  }

  // onUpdate(item) {
  //   this.db.list('shopping-list').update(item.key, {name: item.name, price: item.price});
  // }

  onEdit(item) {
    this.navCtrl.push('EditPage', {myparam: item});
  }

  onAdd() {
    this.navCtrl.push('AddPage');

  }

  // onListClick(item) {
  //   this.as.create( 
  //     { title: "Menu",	
  //       buttons: [		
  //         { text: "Delete", 		  
  //         role: "destructive", 		  handler: ()=> {console.log(item);}
  //         } ,
  //         { text: "Cancel",
  //       role: "cancel"}
  //       ]
  //     }).present();
  // }
}
